# CONTRIBUTING

In order to contribute to this project you should follow the following rules:

- Create a merge request 
- Pipeline should pass
- Your code should come along with unit tests
- Commits should be small whenever possible
- Try whenever possible to follow the angular commit message convention (https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines)