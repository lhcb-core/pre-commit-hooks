================
Pre-commit hooks
================

This repository contains python pre-commit hooks used for LHCb

How to use pre-commit hooks
---------------------------

In order to use pre-commit hooks, at first you need to install the pre-commit python package (e.g. ``pip install pre-commit``).
Also, you need to enable it in your repository of choice. To do that, navigate inside a directory of the repository and run ``pre-commit install``

Afterwards, you will need to choose the hooks to use. 
In order to do that, you will need a *.pre-commit-config.yaml* at the top level of your repository. Inside the config file, you declare which hooks you want to use from which repositories
An example config file would look like this https://ibb.co/HFLWg3R and another one can be found in the lb_dev repository https://gitlab.cern.ch/lhcb-core/lb_dev/blob/master/.pre-commit-config.yaml

- repos is a list of repositories
- repo can be any link that you can use along with `git clone`
- rev is the commit sha or the tag of the repo you want to use
- hooks are the hooks of the repo that will be used in your project
- id is the id of the hook, which can be found in the hooks configuration of the repo
- verbose true means that print statements inside the hook appear on the screen

You can check more options on what you can have inside your config yaml here (https://pre-commit.com/) in the **.pre-commit-config.yaml - top level** section

**You do not need to clone the hook repository for it to work, all you have to do are the steps above**

How to create your own hooks
----------------------------

To create your own hooks you need to have a repository to contain them and a *.pre-commit-hooks.yaml* where you define your hooks

The hooks you create are basically scripts you write and then link their executable version on the hooks yaml file. 

At the very least, your hook in the yaml needs to contain an id and an entry field  where id is the id you give to the hook (it is not the name, you can also define a name in the name field) and entry is the entrypoint of the hook. Where the executable is. For example, for python hooks, you create a console-script entrypoint and the name you give that console-script is what you need to put into the entry field.

Check more hook options here (https://pre-commit.com/) in the **Creating new hooks** section

You can also see an example hooks yaml in the top level of this repository

Hooks can be created using a lot of different tools, like python, go, docker and more

Running your hooks in Gitlab-CI
-------------------------------

One of the best ways to utilize pre-commit hooks, is by including them in your Gitlab-CI as part of your pipelines.

We highly suggest you do so by including the following lines

.. code-block:: yaml

    include:
      - remote: 'https://gitlab.cern.ch/lhcb-core/pre-commit-hooks/-/raw/master/ci-pre-commit-hooks.yml'

and the stage **pre-commit-hooks** in your `.gitlab-ci.yml`.

For example, if you already have a build and a test stage like so:

.. code-block:: yaml

    stages:
        - build
        - test

you should add the **pre-commit-hooks** stage like so:

.. code-block:: yaml

    stages:
        - pre-commit-hooks
        - build
        - test

`Note: pre-commit-hooks does not need to be the first stage to be executed`

What this will do, is help you run your pre-commit hooks in all your projects in a uniform way and here is how.

What happens behind the scenes is:

- pre-commit is installed
- check whether this repository already has a `.pre-commit-config.yaml` file
- if that file does not exist, a default one with some default hooks is downloaded
- the hooks are run for the changes made from *${TARGET_BRANCH}* to *HEAD*. If target branch is not set, then *master* is used instead