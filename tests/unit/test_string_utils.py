from lhcb_pre_commit_hooks.utils.string_utils import StringUtils

def test_find_strings_in_list():
    strings = ["string1", "string2"]
    list_items = ["item1", {}, "string1", "item3"]

    strings_in_list = StringUtils.find_strings_in_list(strings, list_items)

    assert ["string1"] == strings_in_list


def test_find_strings_in_empty_list():
    strings = ["string1", "string2"]
    list_items = []

    strings_in_list = StringUtils.find_strings_in_list(strings, list_items)

    assert [] == strings_in_list