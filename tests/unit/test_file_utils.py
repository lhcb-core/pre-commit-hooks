from os.path import basename, exists, abspath
from os import mkdir
from shutil import rmtree
import tempfile
from uuid import uuid1

from lhcb_pre_commit_hooks.utils.file_utils import FileUtils


def test_get_filenames_in_path():
    temp_folder = tempfile.mkdtemp(prefix="")
    try:
        temp1 = tempfile.NamedTemporaryFile(
            prefix="temp1", suffix=".txt", dir=temp_folder
        )
        temp2 = tempfile.NamedTemporaryFile(
            prefix="temp2", suffix=".txt", dir=temp_folder
        )
        file_names = [basename(temp1.name), basename(temp2.name)]

        files_in_path = FileUtils.get_filenames(temp_folder)

        assert file_names.sort() == files_in_path.sort()
    finally:
        rmtree(temp_folder)


def test_get_non_empty_filenames():
    # A normal directory is created here because tempfile.TemporaryDirectory()
    # creates a folder-like object, instead of a real directory and
    #  scandir cannot handle it
    # each test that needs a directory uses a uuid to create unique names
    # so that it does not interfere with directories of other tests
    folder_name = 'temp_{}'.format(uuid1())
    if not exists('./{}'.format(folder_name)):
        mkdir("./{}".format(folder_name))
    try:
        temp1 = tempfile.NamedTemporaryFile(
            delete=False, prefix="temp1", suffix=".txt", dir="./{}".format(folder_name)
        )
        temp2 = tempfile.NamedTemporaryFile(
            prefix="temp2", suffix=".txt", dir="./{}".format(folder_name))

        temp1.write(b"some string")
        temp1.close()

        file_names = [basename(temp1.name)]

        non_empty_files_in_path = FileUtils.get_non_empty_filenames(
            "./{}".format(folder_name))
        assert file_names == non_empty_files_in_path
    finally:
        rmtree("./{}".format(folder_name))

def test_is_empty():
    # A normal directory is created here because tempfile.TemporaryDirectory()
    # creates a folder-like object, instead of a real directory and
    #  scandir cannot handle it
    # each test that needs a directory uses a uuid to create unique names
    # so that it does not interfere with directories of other tests
    folder_name = 'temp_{}'.format(uuid1())
    if not exists('./{}'.format(folder_name)):
        mkdir("./{}".format(folder_name))
    try:
        file_name = './{}/some_file.py'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.close()


        assert FileUtils.is_empty(file_name)
    finally:
        rmtree("./{}".format(folder_name))

def test_is_empty_only_spaces_returns_true():
    # A normal directory is created here because tempfile.TemporaryDirectory()
    # creates a folder-like object, instead of a real directory and
    #  scandir cannot handle it
    # each test that needs a directory uses a uuid to create unique names
    # so that it does not interfere with directories of other tests
    folder_name = 'temp_{}'.format(uuid1())
    if not exists('./{}'.format(folder_name)):
        mkdir("./{}".format(folder_name))
    try:
        file_name = './{}/some_file.py'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.write('            ')
        temp_file.close()


        assert FileUtils.is_empty(file_name)
    finally:
        rmtree("./{}".format(folder_name))

def test_is_empty_has_test_returns_false():
    # A normal directory is created here because tempfile.TemporaryDirectory()
    # creates a folder-like object, instead of a real directory and
    #  scandir cannot handle it
    # each test that needs a directory uses a uuid to create unique names
    # so that it does not interfere with directories of other tests
    folder_name = 'temp_{}'.format(uuid1())
    if not exists('./{}'.format(folder_name)):
        mkdir("./{}".format(folder_name))
    try:
        file_name = './{}/some_file.py'.format(folder_name)
        temp_file = open(file_name, "w+")
        temp_file.write('this is some text')
        temp_file.close()


        assert not FileUtils.is_empty(file_name)
    finally:
        rmtree("./{}".format(folder_name))