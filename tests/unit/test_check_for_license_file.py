from lhcb_pre_commit_hooks.hooks.check_for_license_file import check_for_license

from os.path import basename
from os import mkdir
from os.path import exists
from shutil import rmtree, copy
import tempfile
from uuid import uuid1


def test_check_for_license_file():
    # A normal directory is created here because tempfile.TemporaryDirectory()
    # creates a folder-like object, instead of a real directory and
    #  scandir cannot handle it
    # each test that needs a directory uses a uuid to create unique names
    # so that it does not interfere with directories of other tests
    folder_name = 'temp_{}'.format(uuid1())
    if not exists('./{}'.format(folder_name)):
        mkdir("./{}".format(folder_name))
    try:
        temp_file = open("./{}/LICENSE".format(folder_name), "w+")
        temp_file.write("some string")
        temp_file.close()

        # pre-commit hooks need to exit with 0 value if everything worked as it should 
        # and non-zero value if something went wrong
        assert check_for_license('./{}'.format(folder_name)) == 0
    finally:
        temp_file.close()
        rmtree('./{}'.format(folder_name))


def test_check_for_license_file_fail():
    # A normal directory is created here because tempfile.TemporaryDirectory()
    # creates a folder-like object, instead of a real directory and
    #  scandir cannot handle it
    # each test that needs a directory uses a uuid to create unique names
    # so that it does not interfere with directories of other tests
    folder_name = 'temp_{}'.format(uuid1())
    if not exists('./{}'.format(folder_name)):
        mkdir("./{}".format(folder_name))
    try:
        temp_file = open("./{}/random_file_name.rst".format(folder_name), "w+")
        temp_file.write("some string")
        temp_file.close()

        # pre-commit hooks need to exit with 0 value if everything worked as it should 
        # and non-zero value if something went wrong
        assert check_for_license('./{}'.format(folder_name)) != 0
    finally:
        temp_file.close()
        rmtree('./{}'.format(folder_name))
